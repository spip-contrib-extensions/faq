<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
return [
	// T
	'faq_description' => 'Ajoute de nouveaux raccourcis typographiques permettant de décrire de manière simple des <abbr title="Foire Aux Questions">FAQ</abbr> dans un contenu SPIP.

	Les raccourcis doivent être utilisés à l\'intérieur de la balise <code><faq></faq></code>. Une configuration permet d\'ajouter des ancres, de rendre la FAQ dépliable, de charger des styles par défaut...
	La structure HTML produite est, par défaut, toujours basée sur une liste de définitions.',
	'faq_nom'    => 'FAQ',
	'faq_slogan' => 'Créer simplement une FAQ'
];
