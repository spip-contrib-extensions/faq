<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
return [
	// I
	'info_config_faq' => 'Cette configuration s\'applique aux squelettes d\'affichage des FAQ proposés par le plugin, et plus particulièrement aux listes de définitions. Si vous souhaitez définir vos propres affichages pensez à tenir compte de ces paramètres.',

	// L
	'label_charger_css'     => 'Utiliser les styles proposés par défaut par le plugin pour les listes de définitions',
	'label_charger_js'      => 'Utiliser la fonction permettant de plier et déplier les listes de définitions',
	'label_iconifier_js'    => 'Ajouter des puces en début de chaque question pour matérialiser la fonction de pliage/dépliage',
	'label_ancrer_question' => 'Attacher une ancre à chaque question',
	'label_lier_faq'        => 'Insérer un lien vers le début de la FAQ en fin de chaque réponse',
	'legende_ancre_lien'    => 'Ancres, liens et tables des matières',
	'legende_comportement'  => 'Comportement dynamique',
	'legende_css'           => 'Styles',
	'lien_debut_faq'        => 'Retour au début de la FAQ',

	// O
	'outil_inserer_faq'      => 'Insérer une nouvelle FAQ',
	'outil_inserer_question' => 'Insérer une nouvelle question',
	'outil_inserer_titre'    => 'Insérer un titre pour la FAQ',

	// T
	'titre_page_configurer' => 'Configurer le plugin FAQ',
	'titre_form_configurer' => 'Affichages des FAQ',
];
