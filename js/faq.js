const init_faq_dl = function() {
	// Récupérer toutes les questions de FAQ qui ne sont pas encore traitées
	const questions = document.querySelectorAll('dl.faq > dt:not([data-question-loaded])');

	// Boucler sur les questions (dt) et assurer les initialisations et la gestion de l'ouverture/fermeture
	questions.forEach(
		question => {
			// Initialisation des dt, button et dd (ouverture/fermeture et accessibilité)
			// -- Ajouter la classe nécessaire à l'affichage de l'icone idoine (dt)
			question.classList.toggle("item-faq-closed");
			// -- Positionner l'aria expanded (button)
			question.querySelector('button').setAttribute('aria-expanded', 'false');
			// -- Faire disparaitre la réponse et positionner l'attribut aria hidden (dd)
			let reponse = question.nextElementSibling;
			reponse.hidden = true;
			reponse.style.display = 'none';

			// Gérer l'ouverture et la fermeture de la réponse par click sur la question
			question.onclick = function() {
				// Identifier l'état ouvert/fermé de la question avant le changement
				let bouton = question.querySelector('button');
				let est_ouvert = bouton.getAttribute('aria-expanded');
				let reponse = question.nextElementSibling;

				// Agir sur la réponse et son attribut hidden
				reponse.style.display = (est_ouvert === 'true') ? 'none' : 'block';
				reponse.hidden = (est_ouvert === 'true');

				// Inverser la classe du dt pour afficher le bon icone
				question.classList.toggle("item-faq-closed");

				// Assurer la cohérence de l'aria expanded du bouton
				bouton.setAttribute('aria-expanded', (est_ouvert === 'true') ? 'false' : 'true');
			}

			// Indiquer que la question a déjà été traitée pour éviter de le faire n fois
			question.dataset.questionLoaded = true;
		}
	)
}

// Initialisation de la FAQ
if (document.readyState !== "loading") {
	// Si le document est complètement chargé on initialise la FAQ
	init_faq_dl();
	if (window.jQuery) {
		onAjaxLoad(init_faq_dl);
    }
} else {
	// Sinon, le document n'est pas encore chargé, on ajoute l'initialisation de la FAQ
	// au chargement de la page: elle sera lancé en fin de chargement.
	document.addEventListener("DOMContentLoaded", init_faq_dl);
	if (window.jQuery) {
		document.addEventListener("DOMContentLoaded", () => {onAjaxLoad(init_faq_dl)});
	}
}
