<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Insérer la CSS par défaut pour styler la liste d'actions.
 * Si demandé, on insère aussi les styles des dl.
 *
 * @pipeline insert_head_css
 *
 * @param string $flux Code html des styles CSS à charger
 *
 * @return string Code html complété.
 */
function faq_insert_head_css(string $flux) : string {
	// On inclut systématiquement les CSS de base
	$flux .= '<link rel="stylesheet" href="' . timestamp(find_in_path('css/faq.css')) . '" type="text/css" media="all" />';

	// On ajoute si la config le demande les CSS propres aux dl
	include_spip('inc/config');
	$charger_css = lire_config('faq/charger_css');
	if ($charger_css) {
		$flux .= '<link rel="stylesheet" href="' . timestamp(find_in_path('css/faq_dl.css')) . '" type="text/css" media="all" />';
	}

	return $flux;
}

/**
 * Permet d’ajouter des contenus dans la partie `<head>` des pages de l’espace public.
 * Pour le plugin, il permet de charger le js d'ouverture / fermeture des items.
 *
 * @pipeline insert_head
 *
 * @param string $flux Contenu HTML du header
 *
 * @return string Code html complété.
 */
function faq_insert_head(string $flux) : string {
	include_spip('inc/config');
	$charger_js = lire_config('faq/charger_js');
	if ($charger_js) {
		$flux .= '<script src="' . timestamp(find_in_path('js/faq.js')) . '" type="text/javascript"></script>';
	}

	return $flux;
}

/**
 * Ajouter la barre d'outils propre au plugin.
 *
 * @pipeline porte_plume_barre_pre_charger
 *
 * @param array $barres Liste des configurations des barres d'outils
 *
 * @return array Liste mise à jour avec les outils propres au plugin
 */
function faq_porte_plume_barre_pre_charger(array $barres) : array {
	// on ajoute les boutons dans la barre d'édition seulement
	$nom = 'edition';
	$barre = &$barres[$nom];

	$barre->ajouterPlusieursApres(
		'grpCaracteres',
		[
			[
				'id'        => 'faq_sep',
				'separator' => '---------------',
				'display'   => true,
			],
			[
				'id'             => 'faq',
				'name'           => _T('faq:outil_inserer_faq'),
				'className'      => 'outil_faq',
				'openBlockWith'  => "<faq>\n",
				'closeBlockWith' => "\n</faq>",
				'replaceWith'    => "function(h){ return outil_faq(h, '?', true);}",
				'selectionType'  => 'line',
				'display'        => true,
				'dropMenu'       => [
					// bouton ?
					[
						'id'             => 'faq_question',
						'name'           => _T('faq:outil_inserer_question'),
						'replaceWith'    => "function(h){ return outil_faq(h, '?');}",
						'className'      => 'outil_faq_question',
						'selectionType'  => 'line',
						'forceMultiline' => true,
						'display'        => true,
					],
					[
						'id'             => 'faq_titre',
						'name'           => _T('faq:outil_inserer_titre'),
						'replaceWith'    => "function(h){ return outil_faq(h, ':Nouveau titre');}",
						'className'      => 'outil_faq_titre',
						'selectionType'  => 'line',
						'forceMultiline' => true,
						'display'        => true,
					],
				]
			]
		]
	);
	$barre->ajouterFonction(
		"function outil_faq(h, c,recursif) {
			if(recursif){
				// Cas de la sélection de click sur le bouton de création de faq complète
				s = h.selection;
				lines = h.selection.split(/\\r?\\n/);
				var lines_final = [];
				for (j = 0, n = lines.length, i = 0; i < n; i++) {
					// si une seule ligne, on se fiche de savoir qu'elle est vide,
					// c'est volontaire si on clique le bouton
					if (n == 1 || $.trim(lines[i]) !== '') {
						if(r = lines[i].match(/^([+-o]) (.*)$/)){
							r[1] = r[1].replace(/[+-o]/g, c);
							lines_final[j] = r[1]+' '+r[2];
							j++;
						} else {
							lines_final[j] = c + ' '+lines[i];
							j++;
						}
					}
				}
				return lines_final.join('\\n');
			}
			// Click sur les autres boutons
			if ((s = h.selection) && (r = s.match(/^([+-o]) (.*)$/))){
				r[1] = r[1].replace(/[+-o]/g, c);
				s = r[1]+' '+r[2];
			} else {
				s = c + ' '+s;
			}
			return s;
		}"
	);

	return $barres;
}

/**
 * Définir les liens vers les icones affichés dans le porte-plume.
 *
 * @pipeline porte_plume_lien_classe_vers_icone
 *
 * @param array $flux Tableau des liens existants.
 *
 * @return array Tableau des liens mis à jour.
 */
function faq_porte_plume_lien_classe_vers_icone(array $flux) : array {
	return array_merge(
		$flux,
		[
			'outil_faq'          => 'faq-xx.svg',
			'outil_faq_question' => 'faq_question-xx.svg',
			'outil_faq_titre'    => 'faq_titre-xx.svg'
		]
	);
}
